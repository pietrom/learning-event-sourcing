import java.time.ZonedDateTime
import java.util.UUID

import core.events.{BankAccountCreated, EventPublisher}
import org.junit.Test

import scala.math.BigDecimal

class TypeTagTest {
  @Test
  def useTypeTag() : Unit = {
    new EventPublisher().publish(new BankAccountCreated(ZonedDateTime.now, UUID.randomUUID, BigDecimal.valueOf(50)))
  }
}
