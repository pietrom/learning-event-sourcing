package core;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;

import core.events.BankAccountCreated;
import core.events.EventPublisher;
import core.eventsourcing.EventStore;
import core.eventsourcing.InMemoryEventStore;
import core.handlers.AccountHandler;
import core.handlers.CreateBankAccount;
import core.handlers.DepositMoney;
import core.handlers.WithdrawMoney;
import core.model.BankAccount;
import core.model.BankAccountRepository;
import core.model.EventStoreBankAccuntRepository;
import org.junit.Assert;
import org.junit.Test;
import scala.math.BigDecimal;

import java.time.ZonedDateTime;
import java.util.UUID;

public class ScenariosTest {
	@Test
	public void saveAggregateStreamAndReload() {
        EventStore store = new InMemoryEventStore();
        BankAccountRepository repo = new EventStoreBankAccuntRepository(store);
        SystemClock clock = new SystemClock();
        AccountHandler handler = new AccountHandler(clock, repo);
	    UUID id = handler.handle(new CreateBankAccount(clock.now(), "test0", BigDecimal.valueOf(100)));
        handler.handle(new DepositMoney(clock.now(), "test1", id, BigDecimal.valueOf(50)));
        handler.handle(new WithdrawMoney(clock.now(), "test2", id, BigDecimal.valueOf(75)));
        handler.handle(new DepositMoney(clock.now(), "test3", id, BigDecimal.valueOf(50)));

		BankAccount reloaded = repo.load(id);
		Assert.assertThat(reloaded.balance(), is(equalTo(125)));
	}
}
