package core;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.Test;

public class CoreSmokeTest {
	@Test
	public void testName() throws Exception {
		assertThat("Pietro", is(equalTo("Pietro")));
	}
}
