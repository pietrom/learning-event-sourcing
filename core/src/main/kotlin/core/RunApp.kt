package core

import core.eventsourcing.InMemoryEventStore
import core.handlers.AccountHandler
import core.model.EventStoreBankAccuntRepository

fun main(args : Array<String>) {
    val store = InMemoryEventStore()
    val repo = EventStoreBankAccuntRepository(store)
    val handler = AccountHandler(SystemClock(), repo)
    handler.runUseCase()
}