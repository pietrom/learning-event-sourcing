package core.eventsourcing;

import core.events.DomainEvent;
import core.events.EventStream;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EventSourcedResource {
    private List<DomainEvent> changes;
    private int originalVersion;
    private int version;

    public List<DomainEvent> uncommittedChanges() {
        return Collections.unmodifiableList(changes);
    }

    public void commit() {
        changes.clear();
        this.originalVersion = version;
    }

    public int version() {
        return version;
    }

    public int originalVersion() {
        return originalVersion;
    }

    protected EventSourcedResource() {
        this(new ArrayList<DomainEvent>(), 1);
    }

    protected EventSourcedResource(EventStream stream) {
        this(stream.events(), stream.version());
    }

    private EventSourcedResource(List<DomainEvent> events, int version) {
        this.changes = new ArrayList<>();
        for(DomainEvent event : events) {
            apply(event);
        }
        this.version = version;
        this.originalVersion = version;
    }

    private void apply(DomainEvent event) {
        try {
            final Method method = getClass().getDeclaredMethod("on", new Class [] {event.getClass()});
            method.setAccessible(true);
            method.invoke(this, event);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    protected void applyAndPublish(DomainEvent event) {
        apply(event);
        changes.add(event);
        this.version++;
    }
}
