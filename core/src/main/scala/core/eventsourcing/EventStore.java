package core.eventsourcing;

import core.events.DomainEvent;
import core.events.EventStream;
import core.events.EventStreamId;

import java.util.List;

public interface EventStore {
    EventStream loadEventStream(EventStreamId id);

    void appendToStream(EventStreamId aStartingIdentity, List<DomainEvent> anEvents, int expectedVersion);
}
