package core.eventsourcing;

import core.events.DomainEvent;
import core.events.EventStream;
import core.events.EventStreamId;

import java.util.*;
import java.util.stream.Collectors;

public class InMemoryEventStore implements EventStore {
    private List<JournalItem> items = new LinkedList<>();
    @Override
    public EventStream loadEventStream(EventStreamId id) {
        List<DomainEvent> events = items.stream().filter(x -> x.matches(id)).sorted(new Comparator<JournalItem>() {
            @Override
            public int compare(JournalItem o1, JournalItem o2) {
                return o1.getVersion() - o2.getVersion();
            }
        }).map(x -> x.getEvent()).collect(Collectors.toList());

        return new EventStream(events.size(), events);
    }

    @Override
    public void appendToStream(EventStreamId id, List<DomainEvent> events, int expectedVersion) {
        for(DomainEvent evt : events) {
            items.add(new JournalItem(id, ++expectedVersion, evt));
        }
    }
}
