package core.eventsourcing;

import core.events.EventStream;

public class Aggregate extends EventSourcedResource {
    public Aggregate(EventStream stream) {
        super(stream);
    }

    public Aggregate() {
        super();
    }
}
