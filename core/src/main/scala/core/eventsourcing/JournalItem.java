package core.eventsourcing;

import core.events.DomainEvent;
import core.events.EventStreamId;

public class JournalItem {
    private EventStreamId id;
    private int version;
    private DomainEvent event;

    public JournalItem(EventStreamId id, int version, DomainEvent event) {
        this.id = id;
        this.version = version;
        this.event = event;
    }

    public DomainEvent getEvent() {
        return event;
    }

    public int getVersion() {
        return version;
    }

    public boolean matches(EventStreamId id) {
        return this.id.streamId().equals(id.streamId())
                && this.id.streamType().equals(id.streamType());
    }
}
