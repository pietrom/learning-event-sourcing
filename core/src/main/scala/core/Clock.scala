package core

import java.time.ZonedDateTime

trait Clock {
  def now : ZonedDateTime
}

class SystemClock extends Clock {
  override def now: ZonedDateTime = ZonedDateTime.now()
}
