package core.model;

import java.util.UUID;

public interface BankAccountRepository {
    public BankAccount load(UUID id);

    public void save(BankAccount account);
}
