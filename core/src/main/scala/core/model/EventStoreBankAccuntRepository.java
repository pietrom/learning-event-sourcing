package core.model;

import core.events.EventStream;
import core.events.EventStreamId;
import core.eventsourcing.EventStore;

import java.util.UUID;

public class EventStoreBankAccuntRepository implements BankAccountRepository {
    private EventStore store;

    public EventStoreBankAccuntRepository(EventStore store) {
        this.store = store;
    }

    public BankAccount load(UUID id) {
        EventStreamId streamId = new EventStreamId("BankAccount", id.toString());
        EventStream stream = store.loadEventStream(streamId);
        BankAccount account = new BankAccount(stream);
        return account;
    }

    public void save(BankAccount account) {
        EventStreamId streamId = new EventStreamId("BankAccount", account.id().toString());
        store.appendToStream(streamId, account.uncommittedChanges(), account.originalVersion());
    }
}
