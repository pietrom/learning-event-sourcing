package core.model

import java.time.ZonedDateTime
import java.util.UUID

import core.Clock
import core.events._
import core.eventsourcing.Aggregate

class BankAccount(stream: EventStream) extends Aggregate(stream) {
  private var _id: UUID = _
  private var _balance: BigDecimal = _

  def this(id:UUID, balance: BigDecimal)(implicit when: ZonedDateTime) = {
    this(new EventStream())
    applyAndPublish(new BankAccountCreated(when, id, balance))
  }

  def deposit(amount:BigDecimal)(implicit when:ZonedDateTime) = {
    applyAndPublish(new MoneyDeposited(when, _id, amount))
  }

  def withdraw(amount:BigDecimal)(implicit clock:Clock) = {
    applyAndPublish(new MoneyWithdrawn(clock.now, _id, amount))
  }

  private def on(event: BankAccountCreated): Unit = {
    this._id = event.id
    this._balance = event.initialBalance
  }

  private def on(event: MoneyDeposited): Unit = {
    this._balance = this._balance + event.amount
  }

  private def on(event: MoneyWithdrawn): Unit = {
    this._balance = this._balance - event.amount
  }

  def id = _id
  def balance = _balance
}
