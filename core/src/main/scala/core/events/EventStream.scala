package core.events

import java.util

class EventStream(val version: java.lang.Integer, val events: java.util.List[DomainEvent]) {
  def this() {
    this(0, new util.ArrayList[DomainEvent]())
  }
}
