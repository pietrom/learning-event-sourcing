package core.events

import scala.collection.mutable
import scala.reflect.runtime.universe._

trait EventBroker {
  def publish[T <: DomainEvent](event: T)(implicit tag: TypeTag[T])

  def subscribe[T <: DomainEvent](handler: T => ())(implicit tag: TypeTag[T])
}

class InMemoryEventBroker extends EventBroker {
  private val handlers: mutable.Map[Class[_], DomainEvent => ()] = mutable.Map[Class[_], DomainEvent => ()]()

  def publish[T <: DomainEvent](event: T)(implicit tag: TypeTag[T]) = {
    val mirror = tag.mirror
    val clazz : java.lang.Class[_] = mirror.runtimeClass(tag.tpe.typeSymbol.asClass)
    println(clazz)
    println(clazz.getCanonicalName())
  }

  def subscribe[T <: DomainEvent](handler: T => ())(implicit tag: TypeTag[T]) = {
    val mirror = tag.mirror
    val clazz : java.lang.Class[_] = mirror.runtimeClass(tag.tpe.typeSymbol.asClass)
    handlers.put(clazz, event => handler(event.asInstanceOf[T]))
  }
}
