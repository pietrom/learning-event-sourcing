package core.events

import java.time._
import java.util.UUID

trait DomainEvent {
  def occurredOn: ZonedDateTime
}

case class NamedEvent(val occurredOn: ZonedDateTime, val name: String) extends DomainEvent

case class BankAccountCreated(val occurredOn: ZonedDateTime, id: UUID, initialBalance: BigDecimal) extends DomainEvent

case class MoneyDeposited(val occurredOn: ZonedDateTime, id: UUID, amount: BigDecimal) extends DomainEvent

case class MoneyWithdrawn(val occurredOn: ZonedDateTime, id: UUID, amount: BigDecimal) extends DomainEvent
