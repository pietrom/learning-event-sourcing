package core.handlers
import java.time.ZonedDateTime

import collection.JavaConverters._
import java.util.UUID

import core.Clock
import core.model.{BankAccount, BankAccountRepository, EventStoreBankAccuntRepository}

class AccountHandler(clock: Clock, repo: BankAccountRepository) {
  private implicit val _clock: Clock = clock

  def handle(cmd: CreateBankAccount) : UUID = {
    implicit val now:ZonedDateTime = clock.now
    val id = UUID.randomUUID()
    val account = new BankAccount(id, cmd.initialBalance)
    repo.save(account)
    return id
  }

  def handle(cmd: DepositMoney) : Unit = {
    implicit val now:ZonedDateTime = clock.now
    val account = repo.load(cmd.id)
    account.deposit(cmd.money)
    repo.save(account)
  }

  def handle(cmd: WithdrawMoney) : Unit = {
    implicit val now:ZonedDateTime = clock.now
    val account = repo.load(cmd.id)
    account.withdraw(cmd.money)
    repo.save(account)
  }

  def runUseCase: Unit = {
//    implicit val now:ZonedDateTime = clock.now
//    val id = UUID.randomUUID()
//    val account = new BankAccount(id, BigDecimal.valueOf(100));
//    account.deposit(java.math.BigDecimal.valueOf(50))
//    account.withdraw(java.math.BigDecimal.valueOf(75))
//    account.deposit(java.math.BigDecimal.valueOf(50))
//
//    println("Final balance: " + account.balance)
//    println("Events to be persisted:\n" + account.uncommittedChanges().asScala.map(x => x.toString).fold("")((acc, curr) => acc + curr + "\n"))
//
//    val store = new InMemoryEventStore()
//    val repo = new EventStoreBankAccuntRepository(store)
//    repo.save(account)
//
//    val reloaded = repo.load(account.id)
//    println("Final balance: " + reloaded.balance)
  }
}

trait Command {
  def occurredOn : ZonedDateTime
  def issuedBy : String
}

class CreateBankAccount(val occurredOn: ZonedDateTime, val issuedBy: String, val initialBalance: BigDecimal) extends Command

class DepositMoney(val occurredOn: ZonedDateTime, val issuedBy: String, val id: UUID, val money: BigDecimal) extends Command

class WithdrawMoney(val occurredOn: ZonedDateTime, val issuedBy: String, val id: UUID, val money: BigDecimal) extends Command