package core


import org.hamcrest.MatcherAssert.assertThat

import org.junit.Test

import org.hamcrest.CoreMatchers.*
import org.hamcrest.core.Is.`is`
import java.time.ZonedDateTime

import core.events.*

class NamedEventTest {
    @Test
    fun namedEventHasName() {
        val event = NamedEvent(ZonedDateTime.now(), "Pietro")
        assertThat(event.name(), `is`(equalTo("Pietro")))
    }
}